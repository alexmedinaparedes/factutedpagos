const { guessProductionMode } = require("@ngneat/tailwind");

process.env.TAILWIND_MODE = guessProductionMode() ? 'build' : 'watch';

module.exports = {

    prefix: '',
    mode: 'jit',
    purge: {
        content: [
            './src/**/*.{html,ts,css,scss,sass,less,styl}',
        ]
    },
    darkMode: 'class', // or 'media' or 'class'
    theme: {
        fontSize: {
            'xs': '.75rem',
            'ven': '.840rem',
            'sm': '.875rem',
            'tiny': '.875rem',
            'am': '0.932rem',
            'base': '1rem',
            'lg': '1.125rem',
            'xl': '1.25rem',
            '2xl': '1.5rem',
            '3xl': '1.875rem',
            '4xl': '2.25rem',
            '5xl': '3rem',
            '6xl': '4rem',
            '7xl': '5rem',
        },

        fontWeight: {
            hairline: 100,
            'extra-light': 100,
            thin: 200,
            light: 300,
            normal: 400,
            medium: 500,
            grande: 590,
            semibold: 600,
            bold: 700,
            extrabold: 800,
            'extra-bold': 800,
            black: 900,
        },

        borderWidth: {
            default: '1px',
            '0': '0',
            '1': '1px',
            '1.5': '0.11em',
            '2': '2px',
            '3': '3px',
            '4': '4px',
            '6': '6px',
            '8': '8px',
        },

        extend: {

            height: {
                ce: '500px',
                mp: '600px',
                sm: '8px',
                md: '16px',
                lg: '24px',
                xl: '48px',

                '83': '23rem',
                '84': '25rem',
                '85': '27rem',
                '95':'39rem',
                '97': '40rem',
                '98': '41rem',
                '99': '50rem',
                '100':'60rem'
            },

            width: {
                '1/7': '14.2857143%',
                '2/7': '28.5714286%',
                '3/7': '42.8571429%',
                '4/7': '57.1428571%',
                '5/7': '71.4285714%',
                '6/7': '85.7142857%',
                '27': '10rem',
                '38': '13rem',
                '40': '17.5rem',
                '92': '29rem',
                '94': '30rem',
                '95': '32rem',
                '99': '34.875rem',
                '99.4': '40.625',
                '100': '41.813rem',
                '110': '42rem',
                '120': '45rem',
                '123': '46.5rem',
                '130': '48rem',
                '135': '60rem'
            },

            colors: {
                'verde': {
                    '500': '#01B647'
                },

                'oscuro': {
                    '300': '#545454',
                    '400': '#505050',
                    '420': '#3F3F3F',
                    '450': '#2B2B2B',
                    '480': '#3A3A3A',
                    '500': '#3C3C3C',
                    '550': '#2B2B2B',
                    '600': '#242424',
                    '650': '#262626',
                    '750': '#666363',
                    '850': '#263238',
                    '900': '#000000'
                },

                'rojo': {
                    '50': '#FFEAEA',
                    '150': '#EB5050'
                },

                'plomo': {
                    '150': '#e7e2e2',
                    '190': '#777777',
                    '195': '#F2F2F2',
                    '200': '#FAFAFA',
                    '250': '#E9E9E9',
                    '260': '#7D858E',
                    '270': '#6D6D6D',
                    '280': '#D0D0D0',
                    '290': '#A8A8A8',
                    '300': '#D8D8D8',
                    '330': '#7B7B7B',
                    '350': '#C9C9C9',
                    '400': '#A7A7A7',
                    '450': '#D4D4D4',
                    '500': '#BFBEBE'
                },

                'azul': {
                    '50': '#E7F3FF',
                    '300': '#2DA6FE',
                    '400': '#2DA6FE',
                    '450': '#2DA6FE',
                    '600': ' #457CE3'
                },

                'bernar': {
                    '20': '#2DA6FE',
                    '30': '#01B647',
                    '60': '#E7F3FF',
                    '70': '#FF5652',
                },
            },

            fontFamily: {
                Roboto: ['Roboto'],
                Poppins: ['Poppins'],
                Manrope: ['Manrope']
            }

        },
    },
    variants: {
        extend: {},
    },
    plugins: [require('@tailwindcss/aspect-ratio'), require('@tailwindcss/forms'), require('@tailwindcss/line-clamp'), require('@tailwindcss/typography')],
};