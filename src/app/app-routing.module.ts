import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { adminRoutingModule } from './pages/admin/admin.routing';
import { PagesRoutingModule } from './pages/pages-routing.module';



const rutas: Routes = [
  {path:'login', component:LoginComponent},
  {path:'**',redirectTo:'login', pathMatch: 'full'},
];

@NgModule({
  imports: [  RouterModule.forRoot(rutas,{useHash:true}),          
              PagesRoutingModule,
              adminRoutingModule
              
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
