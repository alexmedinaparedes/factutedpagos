import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-siderbar',
  templateUrl: './siderbar.component.html',
  styleUrls: ['./siderbar.component.css']
})
export class SiderbarComponent implements OnInit {
  ngOnInit(): void {
  }


  openTab1: boolean = false;
  openTab2: boolean = false;
  

  changetabs() {
    this.openTab1 = false;
    this.openTab2 = false;
   

  }

  toggleTabs($tabNumber: number) {
    this.changetabs();
    if ($tabNumber === 1) this.openTab1 = true;
    else if ($tabNumber == 2) this.openTab2 = true;
   
  }


  constructor(private router: Router) {
    const ruta = this.router.url;
    if (ruta.includes('mantenimiento')) this.openTab1 = true;
    else if (ruta.includes('reportecertificado')) this.openTab2 = true;

  }

}
