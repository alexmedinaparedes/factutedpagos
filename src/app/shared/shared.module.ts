import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NavbarprincipalComponent } from './navbarprincipal/navbarprincipal.component';
import { FooterComponent } from './footer/footer.component';
import { AppRoutingModule } from '../app-routing.module';
import { SiderbarComponent } from './siderbar/siderbar.component';
import { NavbaradminComponent } from './navbaradmin/navbaradmin.component';



@NgModule({
  // Lo componentes Declaros dentro del modulo shared
  declarations: [
    // Componentes
    NavbarprincipalComponent,
    FooterComponent,
    SiderbarComponent,
    NavbaradminComponent,
  
  ],

  imports: [
    CommonModule,
    AppRoutingModule
  ],

  exports:[
    NavbarprincipalComponent,
    FooterComponent,  
     SiderbarComponent,
    NavbaradminComponent,
  ]

  

})
export class SharedModule { }
