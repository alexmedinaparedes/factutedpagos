import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PagarahoraComponent } from '../modals/pagarahora/pagarahora.component';
import { SubirvoucherComponent } from '../modals/subirvoucher/subirvoucher.component';

@Component({
  selector: 'app-inicioweb',
  templateUrl: './inicioweb.component.html'
})
export class IniciowebComponent implements OnInit {

 
  
  cerrar:boolean = true;

  constructor(public modal:MatDialog) { }

  ngOnInit(): void {
  }

  onAbrirVoucher(){
    this.modal.open(SubirvoucherComponent)
  }

  onPagarAhora(){
    this.modal.open(PagarahoraComponent)
  }
  

  onCerrar(){
    this.cerrar = false;
  }

}
