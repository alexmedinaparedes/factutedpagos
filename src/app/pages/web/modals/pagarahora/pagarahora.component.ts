import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog'

@Component({
  selector: 'app-pagarahora',
  templateUrl: './pagarahora.component.html',
  styles: [
  ]
})
export class PagarahoraComponent implements OnInit {

  constructor(public modal:MatDialog) { }

  ngOnInit(): void {
  }

  card = [
    { nombre: 'assets/img/modals/pagarahora/tarjetas/american.svg' }, 
    { nombre: 'assets/img/modals/pagarahora/tarjetas/bonus.svg'}, 
    { nombre: 'assets/img/modals/pagarahora/tarjetas/cencosud.svg' }, 
    { nombre: 'assets/img/modals/pagarahora/tarjetas/cmr.svg' }, 
    { nombre: 'assets/img/modals/pagarahora/tarjetas/diners.svg'}, 
    { nombre: 'assets/img/modals/pagarahora/tarjetas/discover.svg'},
    { nombre: 'assets/img/modals/pagarahora/tarjetas/jcb.svg'}, 
    { nombre: 'assets/img/modals/pagarahora/tarjetas/maestro.svg'}, 
    { nombre: 'assets/img/modals/pagarahora/tarjetas/mastercard.svg'}, 
    { nombre: 'assets/img/modals/pagarahora/tarjetas/oh.svg'}, 
    { nombre: 'assets/img/modals/pagarahora/tarjetas/provis.svg'}, 
    { nombre: 'assets/img/modals/pagarahora/tarjetas/sodex.svg'}, 
    { nombre: 'assets/img/modals/pagarahora/tarjetas/union.svg'}, 
    { nombre: 'assets/img/modals/pagarahora/tarjetas/visa.svg'}, 
    { nombre: 'assets/img/modals/pagarahora/tarjetas/banco.svg'}, 
    { nombre: 'assets/img/modals/pagarahora/tarjetas/eden.svg'}, 
  ]

}
