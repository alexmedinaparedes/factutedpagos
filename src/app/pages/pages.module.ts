import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagesRoutingModule } from './pages-routing.module';
import { InicioComponent } from './inicio.component';
import { SharedModule } from '../shared/shared.module';
import { AppRoutingModule } from '../app-routing.module';
import { DemoMaterialModule } from '../material-module';
import { RouterModule } from '@angular/router';
import { SubirvoucherComponent } from './web/modals/subirvoucher/subirvoucher.component';
import { EnviadoexitosamenteComponent } from './web/modals/enviadoexitosamente/enviadoexitosamente.component';
import { PagarahoraComponent } from './web/modals/pagarahora/pagarahora.component';
import { IniciowebComponent } from './web/inicioweb/inicioweb.component';




@NgModule({
  declarations: [
         InicioComponent,
         SubirvoucherComponent,
         EnviadoexitosamenteComponent,
         PagarahoraComponent,
         IniciowebComponent,
        
      
         
  ],
  exports:[
  ],
  imports: [
    CommonModule,
    PagesRoutingModule,
    PagesRoutingModule,
    SharedModule,
    AppRoutingModule, 
    DemoMaterialModule,
    RouterModule,
    
  ],
  
})
export class PagesModule { }
