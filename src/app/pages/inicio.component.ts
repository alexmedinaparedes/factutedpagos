import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog'
import { PagarahoraComponent } from './web/modals/pagarahora/pagarahora.component';
import { SubirvoucherComponent } from './web/modals/subirvoucher/subirvoucher.component';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styles: [
  ]
})
export class InicioComponent implements OnInit {

  cerrar:boolean = true;

  constructor(public modal:MatDialog) { }

  ngOnInit(): void {
  }

  onAbrirVoucher(){
    this.modal.open(SubirvoucherComponent)
  }

  onPagarAhora(){
    this.modal.open(PagarahoraComponent)
  }

  onCerrar(){
    this.cerrar = false;
  }
}
