import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { DemoMaterialModule } from 'src/app/material-module';
import { MantenimientoComponent } from './mantenimiento/mantenimiento.component';
import { GestionpagosComponent } from './gestionpagos/gestionpagos.component';
import { AdminComponent } from './admin.component';
import { RouterModule } from '@angular/router';



@NgModule({

  declarations: [
    MantenimientoComponent,
    GestionpagosComponent,
    AdminComponent
  ],

  exports:[
    MantenimientoComponent,
    GestionpagosComponent,
    AdminComponent
  ],
  
  imports: [
    CommonModule, 
    SharedModule, 
    DemoMaterialModule,
     RouterModule
  ]
})
export class AdminModule { }
