import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionpagosComponent } from './gestionpagos.component';

describe('GestionpagosComponent', () => {
  let component: GestionpagosComponent;
  let fixture: ComponentFixture<GestionpagosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GestionpagosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionpagosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
