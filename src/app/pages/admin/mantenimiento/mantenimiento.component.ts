import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mantenimiento',
  templateUrl: './mantenimiento.component.html',
  styleUrls: ['./mantenimiento.component.css']
})
export class MantenimientoComponent implements OnInit {

  displayedColumns: string[] = ['empresa', 'ruc', 'contacto', 'correo', 'opciones'];
  dataSource = ELEMENT_DATA;
  constructor() { }

  ngOnInit(): void {
  }

}
export interface PeriodicElement {
  name: string;
  position: string;
  weight: number;
  symbol: string;
  
}

const ELEMENT_DATA: PeriodicElement[] = [
  {position: 'Globex Corporation', name: '20193193131 ', weight: 3232322242, symbol: 'jackson.graham@example.com'},
  {position: 'Globex Corporation', name: '20193193131 ', weight: 3232322242, symbol: 'jackson.graham@example.com'},
  {position: 'Globex Corporation', name: '20193193131 ', weight: 3232322242, symbol: 'jackson.graham@example.com'},
  {position: 'Globex Corporation', name: '20193193131 ', weight: 3232322242, symbol: 'jackson.graham@example.com'},

];
