import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { AdminComponent } from './admin.component';
import { MantenimientoComponent } from './mantenimiento/mantenimiento.component';
import { GestionpagosComponent } from './gestionpagos/gestionpagos.component';



const routes: Routes = [
    { path: 'admin', component: AdminComponent, 

    children: [
        { path: 'mantenimiento', component: MantenimientoComponent },
        { path: 'gestionpagos', component: GestionpagosComponent },
        { path: '**', redirectTo: 'mantenimiento'},
      ] },
   

];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class adminRoutingModule {}
