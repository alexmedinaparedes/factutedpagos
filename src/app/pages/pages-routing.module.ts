import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { InicioComponent } from './inicio.component';
import { IniciowebComponent } from './web/inicioweb/inicioweb.component';

const routes: Routes = [
  {
    path: 'web', component: InicioComponent,
    
    children: [ 
       { path: 'inicio', component : IniciowebComponent} ,
       {path:'**',redirectTo:'inicio', pathMatch: 'full'},
      ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {}
